# ResBos Code

## Installation:
1. Modify the Makefile to match the setup of your system
2. Compile the code running the command: `make`

## Input card:
The `resbos.in` file contains all the runtime parameters. The brief line by line descriptions can be found below.For additional details, please see the comments at the top of the `resbos_root.f` file

1. The options are the number of runs, the number of iterations for the VEGAS warmup run, the number of function calls for the VEGAS warmup run, the number of interations for the VEGAS full run, and the number of function calls for the VEGAS full run.
2. The resummation grid location to be loaded
3. The regular grid location to be loaded
4. A flag to determine if to generate (un/re)weighted events, other options that are for debugging purposes
5. The cuts on the leptons: pTmin, yMin, DeltaR, pTmax, yMax
6. The cuts on the bosons: InvariantMassMin, InvariantMassMax, pTmin, pTmax, yMin, yMax
7. Additional cuts: MTmin, MTmax, Missing ET
8. Luminosity parameter (leave at 1, this is buggy), the top mass, the Higgs mass
9. The output format for the events. Options are:
    1. ROOTNT1
    2. ROOTNT2
    3. StdHEP
    4. Text1
    5. Text2
    6. GBook
10. Slicing parameter to hand poor numerical cancellation as pT -> 0, remaining are parameters for diphoton production
11. Higgs decay mode parameters
12. The W mass and width
13. The Z mass and width
14. Flags to control EW scheme, qed scheme, CKM matrix, Flag to use k-factors for higher order angular function corrections

## Running the code:
Once the run card is setup as desired, the code is run through `./resbos`. The code will load the gridfiles pointed to, and generate events. At the end of the run when in ROOTNT1 or ROOTNT2 output format, a resbos.root file is created containing all of the events. 

For generating events with different PDF weights, it is suggested to use the reweighting scheme implemented in the resbos code. This is achieved by setting the (un/re)weighted events flag to -1 for the central PDF, and then running with -2 for each additional PDF member. This will ensure that the events generated are identical to those from the central PDF.
